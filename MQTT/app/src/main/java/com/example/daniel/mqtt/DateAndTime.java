package com.example.daniel.mqtt;

import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DateAndTime extends AppCompatActivity {
    private TextView TextViewTime,TextVievDate;

    MQTTHelperButton mqttHelper;
    public MqttAndroidClient mqttAndroidClient;
    private TextInputLayout textDate, textTime,textDateOff,TextTimeOff;
    MqttAndroidClient client;
    Calendar calendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date_and_time);

        textDateOff = (TextInputLayout)findViewById(R.id.text_inout_dateoff);
        TextTimeOff = (TextInputLayout)findViewById(R.id.text_inout_timeoff);
        TextViewTime = (TextView)findViewById(R.id.textViewTime);
        TextVievDate = (TextView)findViewById(R.id.textViewDate);
        textDate = (TextInputLayout)findViewById(R.id.text_inout_date);
        textTime = (TextInputLayout)findViewById(R.id.text_inout_time);


        calendar = Calendar.getInstance();


        Thread thread = new Thread() {

            @Override
            public void run() {
                try{
                    while (!isInterrupted()){
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Calendar calendar = Calendar.getInstance();
                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm");
                                SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("dd-MM-yyyy");
                                String currenttime = simpleDateFormat.format(calendar.getTime());
                                String currentdate = simpleDateFormat1.format(calendar.getTime());
                                TextViewTime.setText(currenttime);
                                TextVievDate.setText(currentdate);




                                String time = textTime.getEditText().getText().toString().trim();
                                String date = textDate.getEditText().getText().toString().trim();

                                String timeoff = TextTimeOff.getEditText().getText().toString().trim();
                                String dateoff = textDateOff.getEditText().getText().toString().trim();



                                if(time.equals(TextViewTime.getText().toString())){
                                    String topic = "/feeds/buttononoff";
                                    String payload = "ON";
                                    byte[] encodedPayload = new byte[0];
                                    try {
                                        encodedPayload = payload.getBytes("UTF-8");
                                        MqttMessage message = new MqttMessage(encodedPayload);
                                        client.publish(topic, message);
                                    } catch (UnsupportedEncodingException | MqttException e) {
                                        e.printStackTrace();
                                    }
                                }

                                if(date.equals(TextVievDate.getText().toString())){
                                    String topic = "/feeds/buttononoff";
                                    String payload = "ON";
                                    byte[] encodedPayload = new byte[0];
                                    try {
                                        encodedPayload = payload.getBytes("UTF-8");
                                        MqttMessage message = new MqttMessage(encodedPayload);
                                        client.publish(topic, message);
                                    } catch (UnsupportedEncodingException | MqttException e) {
                                        e.printStackTrace();
                                    }
                                }



                                if(timeoff.equals(TextViewTime.getText().toString())){
                                    String topic = "/feeds/buttononoff";
                                    String payload = "OFF";
                                    byte[] encodedPayload = new byte[0];
                                    try {
                                        encodedPayload = payload.getBytes("UTF-8");
                                        MqttMessage message = new MqttMessage(encodedPayload);
                                        client.publish(topic, message);
                                    } catch (UnsupportedEncodingException | MqttException e) {
                                        e.printStackTrace();
                                    }
                                }

                                if(dateoff.equals(TextVievDate.getText().toString())){
                                    String topic = "/feeds/buttononoff";
                                    String payload = "OFF";
                                    byte[] encodedPayload = new byte[0];
                                    try {
                                        encodedPayload = payload.getBytes("UTF-8");
                                        MqttMessage message = new MqttMessage(encodedPayload);
                                        client.publish(topic, message);
                                    } catch (UnsupportedEncodingException | MqttException e) {
                                        e.printStackTrace();
                                    }
                                }

                            }


                        });
                    }

                }catch (InterruptedException e) {
                    TextViewTime.setText(R.string.app_name);
                }
            }
        };
        thread.start();

        startMqtt();
        String clientId = MqttClient.generateClientId();
        client =
                new MqttAndroidClient(getApplicationContext(), "tcp://164.8.22.199:1883",
                        clientId);

        try {
            IMqttToken token = client.connect();
            token.setActionCallback(new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    // We are connected
                    //Log.d(TAG, "onSuccess");
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    // Something went wrong e.g. connection timeout or firewall problems
                    //Log.d(TAG, "onFailure");

                }
            });
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    private void startMqtt(){


        mqttHelper = new MQTTHelperButton(getApplicationContext());
        mqttHelper.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean b, String s) {

            }

            @Override
            public void connectionLost(Throwable throwable) {

            }

            @Override
            public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
                Log.w("Debug",mqttMessage.toString());


            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

            }
        });
    }


}


