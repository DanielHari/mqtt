package com.example.daniel.mqtt;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;

public class MenuActivity extends AppCompatActivity {

    CardView voice,voltage,amper,buttononOff,datetime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        voice = (CardView)findViewById(R.id.voice);
        voltage = (CardView)findViewById(R.id.voltage);
        amper = (CardView)findViewById(R.id.amper);
        buttononOff = (CardView)findViewById(R.id.buttonOnOff);
        datetime = (CardView)findViewById(R.id.dateandtime);


        voice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, VoiceActivity.class);
                startActivity(intent);
            }
        });

        datetime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, DateAndTime.class);
                startActivity(intent);
            }
        });

        amper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, AmperActivity.class);
                startActivity(intent);

            }
        });

        voltage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MenuActivity.this, VoltageActivity.class);
                startActivity(intent);

            }
        });

        buttononOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MenuActivity.this, ButtonOnOffActivity.class);
                startActivity(intent);

            }
        });
    }
}
